import React, {useEffect, useState} from 'react';
import {useHistory} from 'react-router-dom'
import {Post} from "../../types/post";
import {Button, Col, Form, Input, message, Row} from "antd";
import TextArea from "antd/es/input/TextArea";

interface AdminPostItemProps {
    post: Post
    handleEdit: Function
    handleRemove: Function
    url: string
}

interface Values {
    title: string
    body: string
}

export const AdminEditPost: React.FC<AdminPostItemProps> = ({post, handleEdit, handleRemove, url}) => {
    const history = useHistory();
    const [values, setValue] = useState<Values>({
        title: '',
        body: ''
    });

    useEffect(() => {
        setValue({
            title: post.title,
            body: post.body
        })
    }, [post]);

    const handleChange = (e: { target: HTMLInputElement | HTMLTextAreaElement }): void => {
        const {name, value} = e.target;

        setValue({
            ...values,
            [name]: value,
        })
    };

    const handleSubmit = (e: React.FormEvent): void => {
        e.preventDefault();

        const {title, body} = values;

        if (title.length > 3 && body.length > 10) {

            const newPost: Post = {
                id: url,
                title,
                body,
                image: post.image,
                date: post.date
            };

            handleEdit(newPost)
            message.info('Вы изменили пост')
        } else alert('Пожалуйста заполните все поля')
    };

    const removePost = (e: React.FormEvent): void => {
        e.preventDefault();

        handleRemove();
        history.push('/admin/posts')

        message.info('Вы удалили пост')
    };

    return (
        <Row type="flex" justify="center" align={'middle'}>
            <Col span={8}>
                <h1>Обновите Пост</h1>
                <Form>
                    <Form.Item>
                        <Input size="large" placeholder="Title..." name={'title'} value={values.title} onChange={handleChange} style={{maxWidth: 320}}/>
                    </Form.Item>
                    <Form.Item>
                        <TextArea name={'body'} allowClear value={values.body} onChange={handleChange}/>
                    </Form.Item>
                    <Button size={'large'} type="primary" onClick={handleSubmit} className="login-form-button" style={{marginRight: '1rem'}}>
                        Save
                    </Button>
                    <Button size={'large'} type="danger" onClick={removePost} className="login-form-button">
                        Delete
                    </Button>
                </Form>
            </Col>
        </Row>
    );
};
