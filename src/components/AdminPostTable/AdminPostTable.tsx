import React from 'react';
import {Post} from "../../types/post";
import {NavLink} from "react-router-dom";
import {Table} from "antd";
import {Button} from "antd";

interface AdminPostTableProps {
    posts: Post[]
}

export const AdminPostTable: React.FC<AdminPostTableProps> = ({posts}) => {
    const columns = [
        {
            title: 'Title',
            dataIndex: 'title',
            key: 'title',
        },
        {
            title: 'Date',
            dataIndex: 'date',
            key: 'date',
        },
        {
            title: 'Action',
            key: 'action',
            render: ((post: Post) =>
                    <NavLink to={`/admin/post/${post.id}`}>
                        <Button type="primary" icon="search">Open</Button>
                    </NavLink>
            )
        },
    ];
    return <Table dataSource={posts} columns={columns} rowKey={(record: Post) => record.id}/>;
}
