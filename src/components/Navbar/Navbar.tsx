import React from 'react';
import {AuthContextConsumer} from "../../context/auth/AuthContext";
import app from "../../base";
import {Button, message} from "antd";
import {UserNavbar} from "./UserNavbar/UserNavbar";
import {AdminNavbar} from "./AdminNavbar/AdminNavbar";


export interface Links {
    title: string
    to: string
    id: number
    exact: boolean
    icon: string
}

const links: Links[] = [
    {title: 'Главная', to: '/', id: 0, exact: true, icon: 'home'},
    {title: 'Войти', to: '/auth', id: 2, exact: false, icon: 'login'},
];

const adminLinks: Links[] = [
    {title: 'Посты', to: '/admin/posts', id: 0, exact: true, icon: 'table'},
    {title: 'Создать пост', to: '/admin/create', id: 1, exact: false, icon: 'user'},
];

export const Navbar: React.FC = () => {

    return (
        <AuthContextConsumer>
            {({user}) =>
                <>
                    {!!user ?
                        <Button
                            icon={'logout'}
                            size={'large'}
                            onClick={() => {
                                app.auth().signOut()
                                message.info('Вы вышли из системы')}
                            }
                            style={{
                                position: "fixed",
                                top: '2rem',
                                right: '3rem'
                            }}
                        >
                            Logout
                        </Button>
                        : null
                    }
                    {!!user
                        ? <AdminNavbar user={user} adminLinks={adminLinks} links={links}/>
                        : <UserNavbar user={user} adminLinks={adminLinks} links={links}/>
                    }
                </>
            }
        </AuthContextConsumer>
    );
};
