import React from 'react';
import {Icon, Menu} from "antd";
import {NavLink} from "react-router-dom";
import {Layout} from 'antd'
import {User} from "../../AuthForm/Login/Login";
import {Links} from "../Navbar";

interface AdminNavbarProps {
    user: User
    adminLinks: Links[]
    links: Links[]
}

export const UserNavbar: React.FC<AdminNavbarProps> = ({user, adminLinks, links}) => {
    const {Header} = Layout

    return (
        <Header>
            <Menu theme="dark"
                  mode="horizontal"
                  defaultSelectedKeys={['0']}
                  style={{ lineHeight: '64px' }}
            >
                {!!user
                    ? adminLinks.map(({title, to, id, exact, icon}) => (
                        <Menu.Item key={id}>
                            <NavLink to={to} exact={exact}>
                                <Icon type={icon}/>
                                <span>{title}</span>
                            </NavLink>
                        </Menu.Item>
                    ))
                    : links.map(({title, to, id, exact, icon}) => (
                        <Menu.Item key={id}>
                            <NavLink to={to} exact={exact}>
                                <Icon type={icon}/>
                                <span>{title}</span>
                            </NavLink>
                        </Menu.Item>
                    ))
                }
            </Menu>
        </Header>
    );
};
