import React, {useState} from 'react';
import {Icon, Menu} from "antd";
import {NavLink} from "react-router-dom";
import {Layout} from 'antd'
import {User} from "../../AuthForm/Login/Login";
import {Links} from "../Navbar";

interface AdminNavbarProps {
    user: User
    adminLinks: Links[]
    links: Links[]
}

export const AdminNavbar: React.FC<AdminNavbarProps> = ({user, adminLinks, links}) => {
    const [toggle, setToggle] = useState<boolean>(false)

    const toggleCollapsed = () => {
        setToggle(prevState => prevState = !prevState)
    };

    const {Sider} = Layout

    return (
        <Sider collapsible collapsed={toggle} onCollapse={toggleCollapsed}>
            <div className="logo"/>
            <Menu theme="dark" defaultSelectedKeys={['0']} mode="inline">
                {!!user
                    ? adminLinks.map(({title, to, id, exact, icon}) => (
                        <Menu.Item key={id}>
                            <NavLink to={to} exact={exact}>
                                <Icon type={icon}/>
                                <span>{title}</span>
                            </NavLink>
                        </Menu.Item>
                    ))
                    : links.map(({title, to, id, exact, icon}) => (
                        <Menu.Item key={id}>
                            <NavLink to={to} exact={exact}>
                                <Icon type={icon}/>
                                <span>{title}</span>
                            </NavLink>
                        </Menu.Item>
                    ))
                }
            </Menu>
        </Sider>
    );
};
