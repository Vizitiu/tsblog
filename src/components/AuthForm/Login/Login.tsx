import React, {useCallback} from 'react';
import app from "../../../base";
import {Form, Icon, Input, Button, message} from 'antd';

export interface User {
    email: string
    password: string
}

export const Login: React.FC = () => {
    const handleSubmit = useCallback(async (e: any) => {
        e.preventDefault();

        const {email, password} = e.target.elements

        try {
            await app
                .auth()
                .signInWithEmailAndPassword(email.value, password.value);

            email.value = ''
            email.password = ''
            message.success('Вы успешно вошли')
        } catch (e) {
            message.error('Пожалуйста введите верный данные')
        }
    }, [])

    return (
        <Form onSubmit={handleSubmit} className="login-form">
            <Form.Item>
                <Input
                    prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                    type="email"
                    placeholder="Email"
                    name={'email'}
                />
            </Form.Item>
            <Form.Item>
                <Input
                    prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>}
                    type="password"
                    placeholder="Password"
                    name={'password'}
                />
            </Form.Item>
            <Form.Item>
                <Button type="primary" icon={'login'} htmlType="submit" className="login-form-button">
                    Log in
                </Button>
            </Form.Item>
        </Form>
    );
};
