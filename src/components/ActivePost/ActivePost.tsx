import React from 'react';
import {Post} from "../../types/post";
import {Typography} from 'antd';

interface ActivePostProps {
    post: Post
}

export const ActivePost: React.FC<ActivePostProps> = ({post}) => {
    const {title, body} = post;
    const {Title, Text} = Typography;

    return (
        <Typography style={{padding: '5rem'}}>
            <Title>{title}</Title>
            <Text>
                {body}
            </Text>
        </Typography>
    )
};
