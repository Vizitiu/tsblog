import React from 'react';
import classes from "./Loader.module.scss";

export const Loader: React.FC = () => <div className={classes.WrapperLoader}><div className={classes.Loader}/></div>;
