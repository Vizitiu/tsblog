import React from 'react';
import {PostItem} from "./PostItem/PostItem";
import {Post} from "../../types/post";
import {Col, Row} from "antd";

interface ActivePostsProps {
    posts: Post[]
}

export const ActivePosts: React.FC<ActivePostsProps> = ({posts}) => {

    return (
        <>
            <Row>
                {posts.map(({id, title, body, date, image}) => (
                    <Col xs={24} sm={{span: 12}} md={{span: 12}} lg={{span: 12}} xl={{span: 8}} xxl={{span: 8}}
                         style={{
                             display: 'flex',
                             justifyContent: 'center',
                             marginBottom: '.5rem'
                         }}
                         key={id}
                    >
                        <PostItem id={id} title={title} body={body} date={date} image={image}/>
                    </Col>
                ))}
            </Row>
        </>
    )
}
