import React from 'react';
import {NavLink} from "react-router-dom";
import {Post} from "../../../types/post";
import {Card} from 'antd';

export const PostItem: React.FC<Post> = ({id, title, body, date, image}) => {

    const {Meta} = Card

    return (
        <NavLink to={`/post/${id}`}>
            <Card
                hoverable
                cover={<img alt={title}
                            src={image}/>}
                style={{width: 300}}
            >
                <Meta title={date} description={title}/>
            </Card>
        </NavLink>
    )
}
