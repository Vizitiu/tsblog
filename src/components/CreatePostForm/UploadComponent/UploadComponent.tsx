import React from 'react';
import {Icon, message, Upload} from "antd";
import {UploadFile} from "antd/lib/upload/interface";

interface Image {
    lastModified: number
    name: string
    size: number
    type: string
    uid: string
    webkitRelativePath: string
    slice: any
}

export const UploadComponent: React.FC<any> = ({setImage, image}) => {

    const getBase64 = (img: Image, callback: Function) => {
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
    }

    const beforeUpload = (file: UploadFile) => {
        const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
        if (!isJpgOrPng) {
            message.error('You can only upload JPG/PNG file!');
        }
        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) {
            message.error('Image must smaller than 2MB!');
        }
        return isJpgOrPng && isLt2M;
    }

    const handleUploadFile = (info: any) => {
        if (info.file.status === 'uploading') {
            return
        }
        if (info.file.status === 'done') {
            getBase64(info.file.originFileObj, (imageUrl: any) => {
                setImage(imageUrl)
            });
        }
    };

    const uploadButton = (
        <div>
            <Icon type="plus"/>
            <div className="ant-upload-text">Upload</div>
        </div>
    );

    return (
        <div style={{
                marginLeft: '50px',
        }}>
            <Upload
                action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                listType="picture-card"
                beforeUpload={beforeUpload}
                onChange={handleUploadFile}
            >
                {!!image ? null : uploadButton}
            </Upload>
        </div>
    );
};
