import React, {useState} from 'react';
import classes from "./CreatePostForm.module.scss";
import TextareaAutosize from 'react-textarea-autosize'
import {UploadComponent} from "./UploadComponent/UploadComponent";

interface CreatePostFormProps {
    addPost: Function
}

interface Values {
    title: string
    body: string
}


export const CreatePostForm: React.FC<CreatePostFormProps> = ({addPost}) => {
    const [image, setImage] = useState<null | string>(null);
    const [values, setValue] = useState<Values>({
        title: '',
        body: ''
    });

    const handleChange = (e: { target: HTMLInputElement | HTMLTextAreaElement }) => {
        const {name, value} = e.target;

        setValue({
            ...values,
            [name]: value
        })
    };

    const handleSubmit = (e: React.FormEvent) => {
        e.preventDefault();
        const {title, body} = values;

        if (values.title.length > 3 && values.body.length > 1 && !!image) {

            const post = {
                id: new Date().getTime().toString(),
                title,
                body,
                image,
                date: new Date().toLocaleDateString()
            };

            addPost(post);

            setValue({
                title: '',
                body: ''
            });
            setImage(null);
        } else {
            alert('Пожалуйста заполните все поля')
        }
    };


    return (
        <div className={classes.CreatePostForm}>
            <form onSubmit={handleSubmit}>
                <label>Добавить пост</label>
                <input type="title" name="title" value={values.title} placeholder="title..." onChange={handleChange}/>
                <TextareaAutosize type="title" name="body" value={values.body} placeholder="body..."
                                  onChange={handleChange}/>
                {<UploadComponent image={image} setImage={setImage}/>}
                <input type="submit"/>
            </form>
        </div>
    );
};
