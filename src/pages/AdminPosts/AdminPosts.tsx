import React, {useEffect} from 'react';
import {TypedUseSelectorHook, useDispatch as useDispatchGeneric, useSelector as useSelectorGeneric} from "react-redux";
import {Action, Dispatch} from "redux";
import {AppState} from "../../store";
import {fetchPosts} from "../../store/actions/postAction";
import {Loader} from "../../components/Loader/Loader";
import {AdminPostTable} from "../../components/AdminPostTable/AdminPostTable";
import {Col, Row} from "antd";

export const useSelector: TypedUseSelectorHook<AppState> = useSelectorGeneric;
export const useDispatch: () => Dispatch<Action> = useDispatchGeneric;

const useRedux = () => {
    const dispatch = useDispatch();
    const posts = useSelector(state => state.post.posts);
    const loading = useSelector(state => state.post.loading);
    const error = useSelector(state => state.post.error);

    return {dispatch, posts, error, loading}
};

export const AdminPosts: React.FC = () => {
    const {dispatch, posts, loading, error} = useRedux();

    useEffect(() => {
        dispatch(fetchPosts())
    }, [dispatch]);

    return (
        <>
            {loading
                ? <Loader/>
                : error
                    ? <p>Постов нету</p>
                    : <Row type="flex" justify="center" align={'middle'}>
                        <Col span={16}>
                            < AdminPostTable posts={posts}/>
                        </Col>
                    </Row>
            }
        </>
    );
};
