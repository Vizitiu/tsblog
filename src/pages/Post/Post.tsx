import React, {useEffect} from 'react';
import {TypedUseSelectorHook, useDispatch as useDispatchGeneric, useSelector as useSelectorGeneric} from "react-redux";
import {Dispatch, Action} from 'redux'
import {AppState} from "../../store";
import {ActivePost} from "../../components/ActivePost/ActivePost";
import {searchPost} from "../../store/actions/postAction";
import {Loader} from "../../components/Loader/Loader";

export const useSelector: TypedUseSelectorHook<AppState> = useSelectorGeneric;
export const useDispatch: () => Dispatch<Action> = useDispatchGeneric;

const useRedux = () => {
    const dispatch = useDispatch();
    const post = useSelector(state => state.post.post);
    const loading = useSelector(state => state.post.loading);


    return {dispatch, post, loading}
};

interface MatchParams {
    id: string
}

interface Props extends RouteComponentProps<MatchParams> {

}

interface RouteComponentProps<P> {
    match: match<P>
}

interface match<P> {
    params: P
}

export const Post: React.FC<Props> = ({match}) => {
    const {dispatch, post, loading} = useRedux();

    const url = match.params.id;

    useEffect(() => {
        dispatch(searchPost(url))
    }, [dispatch, url]);

    return (
        <>
            {loading
                ? <Loader/>
                : post
                    ? <ActivePost post={post}/>
                    : <p>Такого поста не существует</p>
            }
        </>
    );
};
