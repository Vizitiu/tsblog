import React, {useEffect} from 'react';
import {TypedUseSelectorHook, useDispatch as useDispatchGeneric, useSelector as useSelectorGeneric} from "react-redux";
import {Dispatch, Action} from 'redux'
import {useHistory} from 'react-router-dom'
import {AppState} from "../../store";
import {editPost, removePost, searchPost} from "../../store/actions/postAction";
import {AdminEditPost} from "../../components/AdminEditPost/AdminEditPost";
import {Loader} from "../../components/Loader/Loader";
import {Post} from "../../types/post";

export const useSelector: TypedUseSelectorHook<AppState> = useSelectorGeneric;
export const useDispatch: () => Dispatch<Action> = useDispatchGeneric;

const useRedux = () => {
    const dispatch = useDispatch();
    const post = useSelector(state => state.post.post);

    return {dispatch, post}
};

interface MatchParams {
    id: string
}

interface Props extends RouteComponentProps<MatchParams> {

}

interface RouteComponentProps<P> {
    match: match<P>
}

interface match<P> {
    params: P
}

export const AdminPost: React.FC<Props> = ({match}) => {
    const {dispatch, post} = useRedux();
    const history = useHistory();

    const url = match.params.id;

    useEffect(() => {
        dispatch(searchPost(url))
    }, [url]);

    const handleEdit = (newPost: Post): void => {
        dispatch(editPost(url, newPost));
        history.push('/admin/posts')
    };

    const handleRemove = (): void => {
        dispatch(removePost(url));
        history.push('/admin/posts')
    };

    return (
        <>
            {post
                ? <AdminEditPost post={post} url={url} handleEdit={handleEdit} handleRemove={handleRemove}/>
                : <Loader/>
            }
        </>
    );
};
