import React, {useState} from 'react';
import {Login} from "../../components/AuthForm/Login/Login";
import {Register} from "../../components/AuthForm/Register/Register";
import {Row, Col} from 'antd'

export const Auth: React.FC = () => {
    const [reg, setReg] = useState(false);

    return (
        <Row style={{marginTop: 150}}>
            <Col span={14} offset={5}>
                {reg
                    ? <Register/>
                    : <Login/>
                }
                {reg
                    ? <p>Если есть аккаунт
                        <span style={{color: 'blue', fontSize: 18, cursor: 'pointer'}}
                              onClick={() => setReg(false)}
                        >&nbsp;нажмите
                    </span>
                    </p>
                    : <p>
                        Если нет аккаунта
                        <span style={{color: 'blue', fontSize: 18, cursor: 'pointer'}}
                              onClick={() => setReg(true)}
                        >&nbsp;нажмите
                    </span>
                    </p>
                }
            </Col>
        </Row>
    );
};
