import React, {Fragment, useEffect} from 'react';
import {ActivePosts} from "../../components/ActivePosts/ActivePosts";
import {TypedUseSelectorHook, useDispatch as useDispatchGeneric, useSelector as useSelectorGeneric} from "react-redux";
import {Action, Dispatch} from "redux";
import {AppState} from "../../store";
import {fetchPosts,} from "../../store/actions/postAction";
import {Loader} from "../../components/Loader/Loader";


export const useSelector: TypedUseSelectorHook<AppState> = useSelectorGeneric;
export const useDispatch: () => Dispatch<Action> = useDispatchGeneric

export const useRedux = () => {
    const dispatch = useDispatch();
    const posts = useSelector(state => state.post.posts);
    const loading = useSelector(state => state.post.loading);
    const error = useSelector(state => state.post.error);

    return {dispatch, posts, error, loading}
};

export const Posts: React.FC = () => {
    const {posts, dispatch, error, loading} = useRedux();

    useEffect(() => {
        dispatch(fetchPosts())
    }, [dispatch]);


    return (
        <Fragment>
            {loading
                ? <Loader/>
                : !!posts
                    ? error
                        ? <p>Что-то пошло не так пожалуйста вернитесь позже</p>
                        : <ActivePosts posts={posts}/>
                    : <h1>На данный момент постов нету</h1>
            }
        </Fragment>
    );
};
