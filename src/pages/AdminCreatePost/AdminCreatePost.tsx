import React from 'react';
import {useDispatch as useDispatchGeneric} from "react-redux";
import {Dispatch, Action} from 'redux'
import {CreatePostForm} from "../../components/CreatePostForm/CreatePostForm";
import {createPost} from "../../store/actions/postAction";
import {Post} from "../../types/post";
import {Row} from "antd";

export const useDispatch: () => Dispatch<Action> = useDispatchGeneric;

const useRedux = () => {
    const dispatch = useDispatch();

    return {dispatch}
};

export const AdminCreatePost: React.FC = () => {
    const {dispatch} = useRedux();

    const addPost = (post: Post): void => {
        dispatch(createPost(post))
    };

    return (
        <Row type="flex" justify="center" align={'middle'}>
            <CreatePostForm addPost={addPost}/>
        </Row>
    );
};
