import {Post} from "./post";

export const FETCH_POSTS = 'FETCH_POSTS';
export const GET_POST = 'GET_POST';
export const SET_LOADING = 'SET_LOADING';
export const SET_ERROR = 'SET_ERROR';
export const CREATE_POST = 'CREATE_POST';
export const REMOVE_POST = 'REMOVE_POST';
export const EDIT_POST = 'EDIT_POST';

export interface FetchPostsAction {
    type: typeof FETCH_POSTS
    posts: Post[]
}

export interface GetPostAction {
    type: typeof GET_POST
    post: Post
}

export interface SetLoadingAction {
    type: typeof SET_LOADING
}

export interface SetErrorAction {
    type: typeof SET_ERROR
}

export interface CreatePostAction {
    type: typeof CREATE_POST
    post: Post
}

export interface RemovePostAction {
    type: typeof REMOVE_POST
    id: number
}

export interface EditPostAction {
    type: typeof EDIT_POST
    post: Post
}

export type PostAction =
    FetchPostsAction |
    GetPostAction |
    SetLoadingAction |
    SetErrorAction |
    CreatePostAction |
    RemovePostAction |
    EditPostAction

export type AppAction = PostAction
