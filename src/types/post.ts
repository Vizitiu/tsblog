export interface Post {
    id: string
    title: string
    body: string
    date: string
    image: string
}

type map<R> = R

export interface PostState<R> {
    posts: [] | Post[] | map<R>
    post: null | Post
    loading: boolean
    error: boolean
}
