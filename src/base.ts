import * as firebase from "firebase";
import 'firebase/auth'

const app = firebase.initializeApp({
    apiKey: "AIzaSyCSjb8qh46QgirpQpybsyyqYZ3T7jw9Z4k",
    authDomain: "tsblog-react.firebaseapp.com",
    databaseURL: "https://tsblog-react.firebaseio.com",
    projectId: "tsblog-react",
    storageBucket: "tsblog-react.appspot.com",
    messagingSenderId: "256530568908",
    appId: "1:256530568908:web:d4c88e9a9e1977ca3c1469"
})

export default app
