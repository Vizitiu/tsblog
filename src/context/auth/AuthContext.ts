import {createContext} from 'react'

export const AuthContext = createContext<null | any>(null);

export const AuthContextProvider = AuthContext.Provider;
export const AuthContextConsumer = AuthContext.Consumer;
