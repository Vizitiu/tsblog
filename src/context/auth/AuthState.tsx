import React, {useEffect, useState} from 'react';
import {AuthContextProvider} from "./AuthContext";
import app from "../../base";

export const AuthState: React.FC = ({children}) => {
    const [user, setUser] = useState<any>(null);

    useEffect(() => {
        app.auth().onAuthStateChanged(setUser)
    }, [user]);

    return (
        <AuthContextProvider value={{user}}>
            {children}
        </AuthContextProvider>
    );
};
