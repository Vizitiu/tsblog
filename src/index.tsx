import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from "react-redux";
import {configStore} from './store'
import './index.scss';
import {App} from './App';
import 'antd/dist/antd.css';


const store = configStore()

const app = (
    <Provider store={store}>
        <App/>
    </Provider>
)

ReactDOM.render(app, document.getElementById('root'));
