import React from 'react';
import {AuthState} from "./context/auth/AuthState";
import {BrowserRouter as Router} from "react-router-dom";
import {Routes} from "./Routes";
import {Navbar} from "./components/Navbar/Navbar";
import {Layout, Icon} from "antd";

export const App = () => {
    const {Footer, Content} = Layout;

    return (
        <AuthState>
            <Router>
                <Layout>
                    <Navbar/>
                    <Layout>
                        <Content style={{marginTop: 100}}>
                            {Routes()}
                        </Content>
                        <Footer>
                            <a href="https://gitlab.com/Vizitiu/tsblog" target={'_blank'}
                               style={{textDecoration: 'none', fontSize: 25}}><Icon type="gitlab"/> GIT REPO</a>
                        </Footer>
                    </Layout>
                </Layout>
            </Router>
        </AuthState>
    );
};

