import React from "react";
import {Switch, Route, Redirect} from 'react-router-dom'
import {Posts} from "./pages/Posts/Posts";
import {Auth} from "./pages/Auth/Auth";
import {AdminPosts} from "./pages/AdminPosts/AdminPosts";
import {Post} from "./pages/Post/Post";
import {AuthContextConsumer} from "./context/auth/AuthContext";
import {AdminPost} from "./pages/AdminPost/AdminPost";
import {AdminCreatePost} from "./pages/AdminCreatePost/AdminCreatePost";

export const Routes = () =>
    <AuthContextConsumer>
        {({user}) => {
            if (!!user) {
                return <Switch>
                    <Route exact path={'/admin/posts'} component={AdminPosts}/>
                    <Route path={'/admin/create'} component={AdminCreatePost}/>
                    <Route path={'/admin/post/:id'} component={AdminPost}/>
                    <Redirect to={'/admin/posts'}/>
                </Switch>
            }

            return <Switch>
                <Route exact path={'/'} component={Posts}/>
                <Route path={'/post/:id'} component={Post}/>
                <Route path={'/auth'} component={Auth}/>
                <Redirect to={'/'}/>
            </Switch>
        }}
    </AuthContextConsumer>

