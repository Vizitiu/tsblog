import {Post, PostState} from "../../types/post";
import {AppAction} from "../../types/postAction";

const STATE: PostState<any> = {
    posts: [],
    post: null,
    loading: false,
    error: false
};

export const post = (state = STATE, action: AppAction): PostState<any> => {
    switch (action.type) {
        case "SET_LOADING":
            return {...state, loading: true, error: false};
        case "SET_ERROR":
            return {...state, loading: false, error: true};
        case "FETCH_POSTS":
            return {...state, posts: action.posts, loading: false, error: false};
        case "GET_POST":
            return {...state, post: action.post, loading: false, error: false};
        case "EDIT_POST":
            return {
                ...state, posts: [...state.posts.map((post: Post) => {
                    if (post.id === action.post.id) return post
                    return post
                })]
            };
        case "REMOVE_POST":
            return {
                ...state,
                post: null,
                posts: [...state.posts.filter((post: Post) => post.id !== action.id.toString())]
            };
        case "CREATE_POST":
            return {...state, posts: [...state.posts, action.post]}
        default:
            return state
    }
};
