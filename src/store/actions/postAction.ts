import {
    CREATE_POST,
    EDIT_POST,
    FETCH_POSTS,
    GET_POST,
    REMOVE_POST,
    SET_ERROR,
    SET_LOADING
} from "../../types/postAction";
import axios from 'axios'
import {Post} from "../../types/post";
import app from "../../base";
import {Dispatch} from "redux";
import {AppState} from "../index";



export const fetchPosts = (): any => async (dispatch: Dispatch) => {
    dispatch(setLoading());

    try {
        const res = await axios.get('https://tsblog-react.firebaseio.com/posts.json');
        const posts: Post[] = [];

        Object.keys(res.data).forEach((id: string) => {
            posts.push({
                ...res.data[id],
                id,
            })
        });

        dispatch({
            type: FETCH_POSTS,
            posts
        })
    } catch (e) {
        dispatch(setError())
    }
};


export const searchPost = (id: string): any => (dispatch: Dispatch, getState: () => AppState) => {
    const state = getState().post;
    const post = state.posts.find((p: Post) => p.id === id);

    dispatch({
        type: GET_POST,
        post
    })
};


export const createPost = (post: Post): any => async (dispatch: Dispatch) => {
    try {
        await axios.post('https://tsblog-react.firebaseio.com/posts.json', post);

        dispatch({
            type: CREATE_POST,
            post
        })
    } catch (e) {
        dispatch(setError())
    }
};

export const editPost = (id: string, post: Post): any => async (dispatch: Dispatch) => {

    try {
        const db = app.database();
        await db.ref('posts/' + id).set(post);

        dispatch({
            type: EDIT_POST,
            post
        })
    } catch (e) {
        dispatch(setError())
    }
};

export const removePost = (id: string): any => async (dispatch: Dispatch) => {
    try {
        const db = app.database();
        await db.ref('posts/' + id).remove();

        dispatch({
            type: REMOVE_POST,
            id
        })
    } catch (e) {
        dispatch(setError())
    }

};

export const setLoading = () => ({type: SET_LOADING});
export const setError = () => ({type: SET_ERROR});
