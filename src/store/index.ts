import {createStore, combineReducers, applyMiddleware} from "redux";
import thunk from "redux-thunk";
import {composeWithDevTools} from "redux-devtools-extension";

import {post} from "./reducer/post";

const rootReducer = combineReducers({
    post
});

export type AppState = ReturnType<typeof rootReducer>

export const configStore = () => {
    const middlewares = [thunk];
    const middleWareEnhancer = applyMiddleware(...middlewares);

    const store = createStore(
        rootReducer,
        composeWithDevTools(middleWareEnhancer)
    );
    return store
};
